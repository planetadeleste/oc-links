<?php namespace PlanetaDelEste\Links;

use Backend;
use System\Classes\PluginBase;

/**
 * Links Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'planetadeleste.links::lang.plugin.name',
            'description' => 'planetadeleste.links::lang.plugin.description',
            'author'      => 'PlanetaDelEste',
            'icon'        => 'icon-link'
        ];
    }

    public function registerNavigation()
    {
        return [
            'links' => [
                'label'       => 'planetadeleste.links::lang.plugin.name',
                'url'         => Backend::url('planetadeleste/links/links'),
                'icon'        => 'icon-link',
                'permissions' => ['planetadeleste.links.*'],
                'order'       => 100,
                'sideMenu'    => [
                    'new_links'    => [
                        'label'       => 'planetadeleste.links::lang.link.new',
                        'icon'        => 'icon-plus',
                        'url'         => Backend::url('planetadeleste/links/links/create'),
                        'permissions' => ['planetadeleste.links.access_links']
                    ],
                    'links'    => [
                        'label'       => 'planetadeleste.links::lang.links.menu_label',
                        'icon'        => 'icon-link',
                        'url'         => Backend::url('planetadeleste/links/links'),
                        'permissions' => ['planetadeleste.links.access_links']
                    ],
                    'categories' => [
                        'label'       => 'planetadeleste.links::lang.categories.menu_label',
                        'icon'        => 'icon-sitemap',
                        'url'         => Backend::url('planetadeleste/links/categories'),
                        'permissions' => ['planetadeleste.links.access_categories']
                    ],
                ]
            ]
        ];
    }

    public function registerPermissions()
    {
        return [
            'planetadeleste.links.access_links'    => [
                'tab'   => 'planetadeleste.links::lang.plugin.name',
                'label' => 'planetadeleste.links::lang.permission.access_links'
            ],
            'planetadeleste.links.access_categories' => [
                'tab'   => 'planetadeleste.links::lang.plugin.name',
                'label' => 'planetadeleste.links::lang.permission.access_categories'
            ],
        ];
    }

    public function registerComponents()
    {
        return [
            'PlanetaDelEste\Links\Components\Links' => 'links'
        ];
    }

}
