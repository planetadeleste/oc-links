<?php namespace PlanetaDelEste\Links\Components;

use Cms\Classes\ComponentBase;
use PlanetaDelEste\Links\Models\Category;
use PlanetaDelEste\Links\Models\Link;

class Links extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'planetadeleste.links::lang.components.links.name',
            'description' => 'planetadeleste.links::lang.components.links.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'category' => [
                'title'       => 'planetadeleste.links::lang.components.links.category',
                'description' => 'planetadeleste.links::lang.components.links.category_description',
                'type'        => 'dropdown',
                'placeholder' => 'planetadeleste.links::lang.components.links.category_placeholder',
                'required'    => true
            ]
        ];
    }

    public function getCategoryOptions()
    {
        return Category::isPublished()->lists('name', 'id');
    }

    public function onRender()
    {
        $this->page['category_id'] = $this->property('category');
        if($this->property('category')) {
            $this->page['category'] = Category::find($this->property('category'));
            $this->page['links'] = Link::whereCategoryId($this->property('category'))->isActive()->orderBy('title', 'asc')->get();
        }
    }

}