<?php namespace PlanetaDelEste\Links\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use PlanetaDelEste\Links\Models\Category;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'PlanetaDelEste.Widgets.Behaviors.ModalController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['planetadeleste.links.access_categories'];
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PlanetaDelEste.Links', 'links', 'categories');
    }

    /**
     * Deleted checked categories.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $categoryId) {
                if (!$category = Category::find($categoryId)) continue;
                $category->delete();
            }

            Flash::success(Lang::get('planetadeleste.links::lang.categories.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('planetadeleste.links::lang.categories.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
