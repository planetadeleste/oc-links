<?php namespace PlanetaDelEste\Links\Models;

use Model;

/**
 * Category Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_links_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'slug'];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'name'];

    public $rules = [
        'name' => 'required'
    ];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'links' => ['PlanetaDelEste\Links\Models\Link']
    ];

    public function scopeIsPublished($query)
    {
        return $query->whereIsPublished(1);
    }
}