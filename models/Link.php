<?php namespace PlanetaDelEste\Links\Models;

use Model;

/**
 * Link Model
 */
class Link extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_links_links';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $rules = [
        'title' => 'required',
        'url' => 'required|url',
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'category' => ['PlanetaDelEste\Links\Models\Category']
    ];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    public function scopeIsActive($query)
    {
        return $query->where('is_active', '1');
    }

}